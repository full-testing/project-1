Инструкция
-------------
Для установки зависимостей выполните:

>npm install

Develop Mode
--------------------
Для запуска development

>set NODE_ENV=development&& gulp

или

>nmp run start:dev

Приложение будет доступно на 8000 порту
([http://localhost:8000](http://localhost:8000)).

Production Mode
--------------------
Для сборки production mode выполните:

>set NODE_ENV=production&& gulp

или

>nmp run start:dev