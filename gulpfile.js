
var gulp        = require('gulp'),
    autoprefixer = require('autoprefixer-stylus'), // Добавление вендорных префиксов (adding of vendor prefixers)
    concat      = require('gulp-concat'), // Объединение файлов (files merger)
    cssnano     = require('gulp-cssnano'),
    csso        = require('gulp-csso'), // Минификация CSS-файлов (minification of CSS files)
    del         = require('del'), // Удаление папок и файлов (delete of folders and files)
    imagemin    = require('gulp-imagemin'), // Оптимизация изображений (images optimization)
    gulpif      = require('gulp-if'),
    htmlmin     = require('gulp-htmlmin'),
    pug         = require('gulp-pug'),
    plumber      = require('gulp-plumber'), // Обработка ошибок (error handling)
    pngquant    = require('imagemin-pngquant'), // Оптимизация PNG-изображений (PNG images optimization)
    //rename      = require('gulp-rename'), // Переименование файлов (files rename)
    removeHtmlComments = require('gulp-remove-html-comments'),
    rigger      = require('gulp-rigger'),
    runSequence = require('run-sequence'),
    sourcemaps  = require('gulp-sourcemaps'), // отладка кода (что было, что стало)
    shorthand   = require('gulp-shorthand'),
    stylus      = require('gulp-stylus'),
    server      = require('gulp-server-livereload'),
    uglify      = require('gulp-uglify'); // Минификация JS-файлов (minification of JS files)

var path = {
    build: { //Куда складывать готовые после сборки файлы
        html:   'public',
        js:     'public/js',
        css:    'public/css',
        img:    'public/img',
        fonts:  'public/fonts',
        server: 'public'
    },
    src: { //Пути откуда брать исходники
        js:     'source/js/main.js',//В стилях и скриптах main файлы
        styl:   'source/styles/main.styl',
        stylAll:'source/**/*.styl',
        pug:    'source/pages/*.pug',
        pugAll: 'source/js/**/*.pug',
        css:    'public/css/*.css',
        img:    'source/assets/img/*.*',
        fonts:  'source/assets/fonts/*.*'
    },
    vendor: {
        css: {
            src: [
                'source/assets/libs/normalize-css/normalize.css',
                'source/assets/libs/bootstrap/dist/css/bootstrap.min.css',
                'source/assets/libs/font-awesome/css/font-awesome.min.css'
            ],
            dest: 'public/css'
        },
        js: {
            src: [
                'source/assets/libs/jquery/dist/jquery.min.js',
                'source/assets/libs/bootstrap/dist/js/bootstrap.min.js'
            ],
            dest: 'public/js'
        },
        fonts: {
            src: [
                'source/assets/libs/font-awesome/fonts/*.*'
            ],
            dest: 'public/fonts'
        }
    },
    watch: { //За изменением каких файлов наблюдать
        pug:    'source/**/*.pug',
        js:     'source/js/**/*.js',
        style:  'source/**/*.styl',
        img:    'source/assets/img/*.*'
    },
    clean: './public'   // Удаление каталога собранного проекта
}

var isProd = process.env.NODE_ENV === 'production';

console.log('Mode |'+process.env.NODE_ENV+'|');

gulp.task('styles', function(){ /*формирование стилей main.css*/
    return gulp.src(path.src.styl)
        .pipe(plumber())
        .pipe(gulpif(!isProd, sourcemaps.init()))
        .pipe( stylus({
            pretty:true,
            use:[
                autoprefixer('last 2 versions')
            ],
            'include css': true
        }))
        .pipe(gulpif(isProd, shorthand()))
        .pipe(gulpif(isProd, cssnano()))
        .pipe(gulpif(!isProd, sourcemaps.write()))
        //.pipe(gulpif(isProd, rename({suffix: '.min'})))
        .pipe(gulp.dest(path.build.css))
});

gulp.task('pages', function(){ /*формирование index.html*/
    return gulp.src(path.src.pug)
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(gulpif(isProd, removeHtmlComments()))
        .pipe(gulpif(isProd, htmlmin({collapseWhitespace: true})))
        .pipe(gulp.dest(path.build.html))
});

gulp.task('js', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(plumber())
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulpif(!isProd, sourcemaps.init())) //Инициализируем sourcemap
        .pipe(gulpif(isProd, uglify())) //Сожмем наш js
        .pipe(gulpif(!isProd, sourcemaps.write())) //Пропишем карты
        //.pipe(gulpif(isProd, rename({suffix: '.min'})))
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
});

// Объединение и минификация CSS-файлов внешних библиотек (task for merger and minification CSS files of libraries, plugins and frameworks)
gulp.task('cssVendor', function () {
    return gulp.src(path.vendor.css.src)
        .pipe(concat('vendor.min.css'))
        .pipe(csso())
        .pipe(gulp.dest(path.vendor.css.dest));
});

// Таск для объединения и минификации JS-файлов внешних библиотек (task for merger and minification JS files of libraries, plugins and frameworks)
gulp.task('jsVendor', function () {
    return gulp.src(path.vendor.js.src)
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.vendor.js.dest));
});

// Таск для объединения папок fonts внешних библиотек (task for merger fonts folders of libraries, plugins and frameworks)
gulp.task('fontsVendor', function () {
    return gulp.src(path.vendor.fonts.src)
        .pipe(gulp.dest(path.vendor.fonts.dest));
});

gulp.task('image', function(){
    return gulp.src(path.src.img)
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.img))
});

gulp.task('fonts', function(){
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('server', function(){
    gulp.src(path.build.server)
        .pipe(gulpif(!isProd, server({
            livereload: true,
            defoltFile: 'index.html',
            directoryListing: false,
            open: true,
            clientConsole: true
        })));
});

gulp.task('watch', ['pages', 'js', 'styles', 'image'], function() {
    gulp.watch([path.watch.pug], ['pages']); /*слежение за изменением содержимого странички*/
    gulp.watch([path.watch.js], ['js']); /*слежение за изменением скриптов*/
    gulp.watch([path.watch.style], ['styles']); /*слежение за изменением стилей*/
    gulp.watch([path.watch.img], ['image']); /*слежение за изменением стилей*/
});

gulp.task('Clean', function(){
    return del(path.clean, {force: true});
});

gulp.task('default', function(){
   if(!isProd){
       runSequence('Clean', 'watch', 'fonts', 'cssVendor', 'jsVendor', 'fontsVendor', 'server');
   }else{
       runSequence(
           'Clean',
           'pages',
           'js',
           'styles',
           'image',
           'fonts',
           'cssVendor',
           'jsVendor',
           'fontsVendor'
       );
   };
});